import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ListPage} from "../list/list";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController) {

    }

    submitSearchForm(){
        // let self = this;
        // this.dataProvider.shelterSearch(this.searchText).then(function(data: any){
        //     self.navCtrl.push(SearchResultsPage, data.hits.hits);
        //     console.log(data);
        // });
    }

    searchWifi(){
        //this.navCtrl.push(AboutPage);
    }


    findShelters(){
        //this.navCtrl.push(ListPage);
        this.navCtrl.push(ListPage);
    }

    showReportCreateModal() {
        //let modal = this.modalCtrl.create(ReportCreatePage);
        //modal.present();
    }

}
