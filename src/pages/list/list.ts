import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import * as algoliasearch from "algoliasearch";
import {Geolocation} from "@ionic-native/geolocation";

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-list',
    templateUrl: 'list.html',
})
export class ListPage {

    listResults: any;
    //aroundLatLng: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation) {
        let self = this;

        var client = algoliasearch('ZK2OLFIMQP', '781ea64211b8349e8ddb934ba33513d5');
        var index = client.initIndex('testing');



        this.geolocation.getCurrentPosition().then((resp) => {
            let location = resp.coords.latitude + ',' + resp.coords.longitude;


            index.search({ aroundLatLng: location, getRankingInfo: true }, function searchDone(err, content) {
                if (err) {
                    console.error(err);
                    return;
                }

                self.listResults = content.hits;
                console.log(content.hits);
                //console.log(self.listResults[0]._rankingInfo);
            });


        }).catch((error) => {
            //let location = "37.11950,-93.25320";

            console.log('Error getting location', error);
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ListPage');
    }

    getMiles(distance: number) {
        let miles = distance * 0.00062137;
        return miles.toFixed(2);
    }

}
